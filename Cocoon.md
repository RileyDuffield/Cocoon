<!--

SPDX-FileCopyrightText: 2020 Riley Duffield <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Cocoon

Sal walked home without her sister. Gathering perfume-wood on her own didn't feel as good as doing it with Charlea.

"Charlea, I'm home," Sal shouted.

"I'm in here," Charlea murmured from her room.

"What's wrong, sis?" Sal's brow furrowed.

"I don't feel too well."

"Well, I brought you perfume-wood. It will make you feel better. It smells so-o-o-o nice."

Sal ran out of the room and tossed some wood on the fire. Sal breathed in the sweet incense, which permeated the home. Sal hoped her sister relished the aroma as much as she did.

"I put the wood on Charlea," Sal bounced into her sister's room. "You should feel better soon."

"I hope so," Charlea said. "Thank you, Sal. Please close my door. I want to be alone."

"Alright," Sal's voice wavered, "Are you sure you want me to go?."

"Yes, I don't want you in here right now."

"I love you," Charlea added.

"I love you too," Sal said as she wiped a tear from her eye.

Sal closed the door and moped by the fire. Wind screamed in through the cracks around the door. Sal shivered. A shaft of moonlight fought through the grimy window to illuminate the home's disrepair.

While Sal pouted, Charlea laboured to spin silk from her wrists. She raced to complete the cocoon before the transition began. Numbness already gnawed at her fingers and toes.

Should she explain it to Sal? Was Sal too little to understand? Did Charlea even understand it herself?

Charlea lacked the energy to explain. Despite her fatigue, she worked all night. At dawn, she finished. The last bit of silk closed over her mouth then she fell asleep permanently.

In the morning, Sal peered into her sister's room. A lifeless cocoon lay in the corner like a wilted fruit. Sal cried. She missed Charlea. She missed her parents. Since the village doctors only knew how to heal humans and cattle, they couldn't help Charlea. Not even the perfume-wood had made her better. Sal's hopes died with her sister.

Sal's parents were gone. They died last winter after caring for their sick neighbors. The Stripe Fever killed them, but spared Charlea and Sal.

Charlea cared for her sister like a mother, at least she did until she got sick. For the past month, she stopped wanting to do anything. She even stopped baking cloudberry pies, her favorite dessert.

As loneliness filled her young heart, Sal didn't know what to do. She kept asking her sister to wake up, but her sister never answered. The fire burned out, as cold as the cocoon.

Sal cried. She wished her parents would come back. No matter how hard she shook the cocoon, her sister wouldn't wake up.

Nobody liked her except her parents and the carpenter's apprentice. Silkwings looked ugly to the villagers. Sal knew she was a monster, that's why outsiders called Silkwings Lacerators. Their fangs and claws frightened people. She was glad her parents had adopted her as a baby anyway.

Sal didn't feel like eating. Tears dragged her into sleep.

Nightmares plagued Sal. Her last conversation with her big sister replayed itself in her head over and over.

A grey glow pushed back the darkness of her dreams and coaxed her awake.

"Little girl, why were you crying?" the glow asked with a feminine voice.

"Where are you?" Sal saw no one in the room.

"I'm at your front door, dear, would you please let me in?"

The voice sounded like it was inside Sal's head, but she opened the door anyway. Light from the doorway chased out the home's gloom.

A beautiful, grey creature stood at the entrance. She had a nice smile and pretty, curly hair. Sal thought she looked trustworthy. Her big, furry ears looked like they would be good at listening.

"Have you eaten tonight, dear?" the woman asked.

"No, I didn't feel like it."

"Well, you sit down and I'll make something nice for you to eat."

The woman knew just where every pan and spice and utensil was. She glided around the kitchen with grace.

"I'm Crystal Comfort," she said as she stirred the soup she made.

"Comfort? Are you here to help me?"

"Yes Sallina. God created me to help people."

Sallina pondered for a while. The woman was very pretty. She had leathery wings and insect-like legs growing out of her back.

"How do you know my name?" Sal asked.

"I've been watching you for a little while. I travel looking for people to help."

"Where did you come from?" Sal asked.

"Another place dear, somewhere you've never heard of."

Sal looked at the wings again.

"Are you an angel?"

Crystal laughed.

"No, dear."

"But, you have wings and you travel to help people?"

"I am not an angel, but you are right. I do travel to different worlds to help people."

Crystal placed two bowls of soup on the kitchen table and sat.

"Ouch!" Sal cried after dropping the spoon. "It's too hot."

"Let me fix that," The woman cooled down the soup with a grey glow that came out of her hand.

Sal sipped the soup.

"This tastes good."

"I'm glad you liked it," Crystal smiled in the same way that Charlea did; or used to.

"Why did you come, Miss Crystal?" Sal asked.

"I came to tell you something very important about your sister."

"But, how do you know about my sister?"

"I know a lot of things, dear. I came to tell you how to help her."

"Why is she in that silk bag?" Sal asked.

"Do you know anything about Silkwings when they grow up?"

"No, Miss Crystal. My parents were humans."

"Well, you see Sal, I am a Silkwing too." Sal noticed Crystal's antennae. "Before a Silkwing grows up, they become sick for a while. It is part of what it means to get older."

Sal sipped more soup. The vegetables and mushrooms tasted so good together.

"How come humans don't get sick before they grow up?"

"I don't know dear, I just know they are made differently from Silkwings."

"How come you look different from me and Charlea?"

"I am a grown up. Grown up Silkwings look very different from when they were children."

"Why are you pretending Charlea will come back? You don't need to hide it from me, I know she is dead. I'm not stupid!" Sal yelled.

Crystal frowned.

"She will come back, dear, if you follow my instructions."

"What instructions?" Sal asked.

"Your sister has been dead for one day. Silkwings take fourteen days to come back to life as a grown up. You must help your sister when that happens."

"How can I help her?" Sal asked.

"Here is a bracelet. When all the stones on the bracelet stop glowing, it will be time to help your sister."

Sal took the bracelet. She ran her fingers over the smooth, stone beads. She counted fourteen beads. One bead was already dim.

"When they all stop glowing, you must help your sister out of her cocoon. She will look very different than when you saw her last."

"Will she be pretty like you?" Sal asked.

"Yes, dear, very pretty."

Sal smiled. She walked around the table and hugged Crystal. Crystal smelled nice and clean. Her warm fur brushed against Sal's cheek.

"Now dear," Crystal cupped Sal's face in her hand. "You must not try to open the cocoon before all the beads stop glowing or your sister will never come back. Do you think you can wait that long?"

"Yes, Miss Crystal."

"Alright, I need to go now, but I will be praying for you. Remember that God is the one who will make your sister all grown up. He is remaking her into a wonderful person inside her cocoon."

"Thank you," Sal said.

"If something really bad happens, put the bracelet on, don't wear it unless you really, really need my help."

Crystal walked to the door.

"I love you, Miss Crystal," Sal said as Crystal turned the knob.

"I love you too, Sallina," the grey glow caressed Charlea's cheek.

After she said that, Crystal disappeared through the doorway.

Sallina felt alone again, but now she had hope.

For two days, Sallina waited patiently before restlessness seized her.

*'She said to put the bracelet on if I needed help.'* Sal thought. *'I feel sad, so I think she wouldn't mind if I put it on.`*

When Sal slipped the bracelet onto her skinny arm, grey light filled the room. Crystal burst into the room with a thunderclap.

Sal trembled before Crystal. She didn't look nice anymore. She looked scary.

"Why did you put the bracelet on when I told you it was only for emergencies?" Crystal boomed.

"I... I..." Sal stammered. "I wanted to talk to you."

Crystal placed her left hand on Sal's shoulder. Crystal's tongue rested behind sharp fangs.

"Sallina," Crystal's voice softened. "I know you feel lonely right now, but you must be patient. I have very important things to do that I cannot tell you about. People might die because the bracelet made me leave them in danger when it called me here."

"But, I just wanted to talk for a few minutes."

"I know dear, but it hurts for me to travel to your world that fast. You have plenty of food and firewood. When I feel bad, I read the holy writings and pray to God. He will help you be patient."

"Are you angry with me?" Sal asked.

"Not anymore. I love you. Please take the bracelet off now."

Sal removed the bracelet. Crystal disappeared with a whoosh.

Sal tried to wait. She wanted to see her sister grown up and beautiful like Crystal said. She wanted it now.

Even though she didn't feel like being patient, she prayed for God to help her. She waited for days.

Thirteen beads were dim now. Sal didn't see the harm in taking a peek inside the cocoon. It was only a day early.

Sal inserted her little fingers between two silk strands near the top of the cocoon. She pried gently, until the silk gave way.

When Sal peered into the hole, it was too dark inside for her to see anything.

She felt bad that she tried to open the cocoon early. Crystal would probably be mad again.

Sal leaned against the wall and cried. She tried to be patient, but she failed.

When Sal glanced back at the cocoon, she noticed a stream of blood coming from the hole she made. Even when she pressed her hand against the cocoon, the bleeding didn't stop.

*'Where is the bracelet?'* Sal panicked.

She raced into the kitchen to look for it. Now she knew Crystal would be mad at her.

Once she found the bracelet, she held it over her hand. Should she put it on?

she put the bracelet on the mantle.

She took it down again.

Crystal might not want to help her after she disobeyed by putting it on.

She took a breath and slipped it on anyway.

Crystal thundered into the room the same way as before. Her wings stretched out, making her look very big and mean.

"What is wrong, Sallina?" Crystal asked.

"I tried to look early," Sal hung her head.

Crystal charged past Sal into Charlea's room. Next to the cocoon, she knelt and placed her fingers on the hole. Tears sprang from Crystal's eyes: strange tears, red, made of blood.

"Will she die?" Sal asked. Her lip quavered.

"She is already dead dear. The question is whether she will come back."

"Will she come back, Miss Crystal?"

"I don't know. You hurt your sister very badly. I will try to help her. Pray."

Sal prayed while Crystal worked to save Charlea. Silk flew from Crystal's wrists while red tears mingled with Charlea's blood. Grey flames danced around the cocoon as Crystal patched the hole.

Crystal leaned against the wall, exhausted. Sal cried into Crystal's chest.

"Did you save her?" Sal asked.

"I don't know dear, let's pray."

Crystal prayed for a long while that God would spare Charlea. Sal was too sad to pray. She just listened until she fell asleep with her head in Crystal's lap.

In the morning, soft noises woke Sal up. The cocoon rustled slightly. Crystal was gone.

Sal looked at the bracelet. None of the beads glowed anymore.

Even though she was scared, she went to the cocoon. She tugged on the silk. It came off more easily than a day earlier. Happiness filled Sal. Her sister was alive again.

A blade came from inside the cocoon and cut the silk. Sal pulled more quickly until all the silk came off.

A slime-covered woman sat in front of Sal. She had the same brown hair as before, but everything else differed. She wasn't a monster anymore.

Sal helped Charlea into the kitchen and found that the water for a bath was already drawn. Crystal must have filled it that morning. After Charlea emerged from the water, she was beautiful. Sal helped her into a new dress that Crystal left as a gift.

As she admired the dress, Sal noticed the scar on Charlea's face in the same place where she tried to open the cocoon.

Sal told her sister everything, including how she disobeyed Crystal.

"I forgive you, Sal; you're my sister," Charlea hugged her.

"But I hurt you," Sal said.

"I know. I should have told you what would happen before I died, but I didn't have the energy to."

"You have a scar because of me," Sal said.

"I forgive you, you are my best friend. A Silkwing gets to choose two people to be best friends for life. Those people get to have a part of that Silkwing living inside them."

"Who will you pick?" Sal asked.

"You. Would you like that?"

"Yes," Sal said.

Charlea's fangs entered into her sister's arm and venom surged in. Next, a symbiont from Charlea entered Sal. It hurt at first, but afterwards filled Sal with joy.

"Who is the other friend?" Sal asked.

"The carpenter's apprentice, Jon. I like him, he's so kind to me."

"Oh, you like him that way. You want him to marry you," Sal said with glee.

"Do you think he'll say yes?"

"Definitely."

The two sisters walked hand in hand to Jon's house. They had never been happier.
