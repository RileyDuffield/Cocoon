---
title:
- type: main
  text: Cocoon
creator:
- role: author
  text: Riley Duffield
- role: illustrator
  text: DiosYubi
date: 08 July 2020
keywords: [children, fairy tale, fable, fantasy, fiction, speculative fiction, creative commons]
rights: Creative Commons Attribution Share Alike 4.0
lang: en
cover-image: CocoonCover.cleaned.png
...
