<!--

SPDX-FileCopyrightText: 2021 Riley Duffield <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Cocoon

Cocoon is a free-culture short story published under CC-BY-SA 4.0 International[^1].

## Synopsis

Sal's sister, Charlea, is sick, very sick. The village doctors can't help. Does
a mysterious woman from another world have the answers to save Charlea?

## Category

* Fiction > Children's books > Fairy tales & fables
* Fiction > Fantasy > Short stories

## Tags

children, fairy tale, fable, fantasy, fiction, speculative fiction, creative commons

[^1]: One file (CocoonCover1.png) is released under a non-free Creative Commons license.
