{\rtf1\ansi\deff0{\fonttbl{\f0 \fswiss Helvetica;}{\f1 Courier;}}
{\colortbl;\red255\green0\blue0;\red0\green0\blue255;}
\widowctrl\hyphauto

{\pard \ql \f0 \sa180 \li0 \fi0 \b \fs36 Cocoon\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Sal walked home without her sister. Gathering perfume-wood on her own didn\u8217't feel as good as doing it with Charlea.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Charlea, I\u8217'm home,\u8221" Sal shouted.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I\u8217'm in here,\u8221" Charlea murmured from her room.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"What\u8217's wrong, sis?\u8221" Sal\u8217's brow furrowed.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I don\u8217't feel too well.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Well, I brought you perfume-wood. It will make you feel better. It smells so-o-o-o nice.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Sal ran out of the room and tossed some wood on the fire. Sal breathed in the sweet incense, which permeated the home. Sal hoped her sister relished the aroma as much as she did.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I put the wood on Charlea,\u8221" Sal bounced into her sister\u8217's room. \u8220"You should feel better soon.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I hope so,\u8221" Charlea said. \u8220"Thank you, Sal. Please close my door. I want to be alone.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Alright,\u8221" Sal\u8217's voice wavered, \u8220"Are you sure you want me to go?.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Yes, I don\u8217't want you in here right now.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I love you,\u8221" Charlea added.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I love you too,\u8221" Sal said as she wiped a tear from her eye.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Sal closed the door and moped by the fire. Wind screamed in through the cracks around the door. Sal shivered. A shaft of moonlight fought through the grimy window to illuminate the home\u8217's disrepair.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 While Sal pouted, Charlea laboured to spin silk from her wrists. She raced to complete the cocoon before the transition began. Numbness already gnawed at her fingers and toes.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Should she explain it to Sal? Was Sal too little to understand? Did Charlea even understand it herself?\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Charlea lacked the energy to explain. Despite her fatigue, she worked all night. At dawn, she finished. The last bit of silk closed over her mouth then she fell asleep permanently.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 In the morning, Sal peered into her sister\u8217's room. A lifeless cocoon lay in the corner like a wilted fruit. Sal cried. She missed Charlea. She missed her parents. Since the village doctors only knew how to heal humans and cattle, they couldn\u8217't help Charlea. Not even the perfume-wood had made her better. Sal\u8217's hopes died with her sister.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Sal\u8217's parents were gone. They died last winter after caring for their sick neighbors. The Stripe Fever killed them, but spared Charlea and Sal.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Charlea cared for her sister like a mother, at least she did until she got sick. For the past month, she stopped wanting to do anything. She even stopped baking cloudberry pies, her favourite dessert.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 As loneliness filled her young heart, Sal didn\u8217't know what to do. She kept asking her sister to wake up, but her sister never answered. The fire burned out, as cold as the cocoon.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Sal cried. She wished her parents would come back. No matter how hard she shook the cocoon, her sister wouldn\u8217't wake up.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Nobody liked her except her parents and the carpenter\u8217's apprentice. Silkwings looked ugly to the villagers. Sal knew she was a monster, that\u8217's why outsiders called Silkwings Lacerators. Their fangs and claws frightened people. She was glad her parents had adopted her as a baby anyway.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Sal didn\u8217't feel like eating. Tears dragged her into sleep.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Nightmares plagued Sal. Her last conversation with her big sister replayed itself in her head over and over.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 A grey glow pushed back the darkness of her dreams and coaxed her awake.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Little girl, why were you crying?\u8221" the glow asked with a feminine voice.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Where are you?\u8221" Sal saw no one in the room.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I\u8217'm at your front door, dear, would you please let me in?\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 The voice sounded like it was inside Sal\u8217's head, but she opened the door anyway. Light from the doorway chased out the home\u8217's gloom.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 A beautiful, grey creature stood at the entrance. She had a nice smile and pretty, curly hair. Sal thought she looked trustworthy. Her big, furry ears looked like they would be good at listening.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Have you eaten tonight, dear?\u8221" the woman asked.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"No, I didn\u8217't feel like it.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Well, you sit down and I\u8217'll make something nice for you to eat.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 The woman knew just where every pan and spice and utensil was. She glided around the kitchen with grace.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I\u8217'm Crystal Comfort,\u8221" she said as she stirred the soup she made.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Comfort? Are you here to help me?\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Yes Sallina. God created me to help people.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Sallina pondered for a while. The woman was very pretty. She had leathery wings and insect-like legs growing out of her back.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"How do you know my name?\u8221" Sal asked.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I\u8217've been watching you for a little while. I travel looking for people to help.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Where did you come from?\u8221" Sal asked.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Another place dear, somewhere you\u8217've never heard of.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Sal looked at the wings again.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Are you an angel?\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Crystal laughed.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"No, dear.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"But, you have wings and you travel to help people?\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I am not an angel, but you are right. I do travel to different worlds to help people.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Crystal placed two bowls of soup on the kitchen table and sat.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Ouch!\u8221" Sal cried after dropping the spoon. \u8220"It\u8217's too hot.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Let me fix that,\u8221" The woman cooled down the soup with a grey glow that came out of her hand.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Sal sipped the soup.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"This tastes good.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I\u8217'm glad you liked it,\u8221" Crystal smiled in the same way that Charlea did; or used to.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Why did you come, Miss Crystal?\u8221" Sal asked.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I came to tell you something very important about your sister.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"But, how do you know about my sister?\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I know a lot of things, dear. I came to tell you how to help her.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Why is she in that silk bag?\u8221" Sal asked.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Do you know anything about Silkwings when they grow up?\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"No, Miss Crystal. My parents were humans.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Well, you see Sal, I am a Silkwing too.\u8221" Sal noticed Crystal\u8217's antennae. \u8220"Before a Silkwing grows up, they become sick for a while. It is part of what it means to get older.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Sal sipped more soup. The vegetables and mushrooms tasted so good together.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"How come humans don\u8217't get sick before they grow up?\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I don\u8217't know dear, I just know they are made differently from Silkwings.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"How come you look different from me and Charlea?\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I am a grown up. Grown up Silkwings look very different from when they were children.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Why are you pretending Charlea will come back? You don\u8217't need to hide it from me, I know she is dead. I\u8217'm not stupid!\u8221" Sal yelled.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Crystal frowned.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"She will come back, dear, if you follow my instructions.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"What instructions?\u8221" Sal asked.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Your sister has been dead for one day. Silkwings take fourteen days to come back to life as a grown up. You must help your sister when that happens.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"How can I help her?\u8221" Sal asked.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Here is a bracelet. When all the stones on the bracelet stop glowing, it will be time to help your sister.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Sal took the bracelet. She ran her fingers over the smooth, stone beads. She counted fourteen beads. One bead was already dim.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"When they all stop glowing, you must help your sister out of her cocoon. She will look very different than when you saw her last.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Will she be pretty like you?\u8221" Sal asked.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Yes, dear, very pretty.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Sal smiled. She walked around the table and hugged Crystal. Crystal smelled nice and clean. Her warm fur brushed against Sal\u8217's cheek.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Now dear,\u8221" Crystal cupped Sal\u8217's face in her hand. \u8220"You must not try to open the cocoon before all the beads stop glowing or your sister will never come back. Do you think you can wait that long?\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Yes, Miss Crystal.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Alright, I need to go now, but I will be praying for you. Remember that God is the one who will make your sister all grown up. He is remaking her into a wonderful person inside her cocoon.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Thank you,\u8221" Sal said.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"If something really bad happens, put the bracelet on, don\u8217't wear it unless you really, really need my help.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Crystal walked to the door.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I love you, Miss Crystal,\u8221" Sal said as Crystal turned the knob.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I love you too, Sallina,\u8221" the grey glow caressed Charlea\u8217's cheek.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 After she said that, Crystal disappeared through the doorway.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Sallina felt alone again, but now she had hope.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 For two days, Sallina waited patiently before restlessness seized her.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 {\i \u8216'She said to put the bracelet on if I needed help.\u8217'} Sal thought. {\i \u8217'I feel sad, so I think she wouldn\u8217't mind if I put it on.`}\par}
{\pard \ql \f0 \sa180 \li0 \fi0 When Sal slipped the bracelet onto her skinny arm, grey light filled the room. Crystal burst into the room with a thunderclap.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Sal trembled before Crystal. She didn\u8217't look nice anymore. She looked scary.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Why did you put the bracelet on when I told you it was only for emergencies?\u8221" Crystal boomed.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I\u8230? I\u8230?\u8221" Sal stammered. \u8220"I wanted to talk to you.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Crystal placed her left hand on Sal\u8217's shoulder. Crystal\u8217's tongue rested behind sharp fangs.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Sallina,\u8221" Crystal\u8217's voice softened. \u8220"I know you feel lonely right now, but you must be patient. I have very important things to do that I cannot tell you about. People might die because the bracelet made me leave them in danger when it called me here.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"But, I just wanted to talk for a few minutes.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I know dear, but it hurts for me to travel to your world that fast. You have plenty of food and firewood. When I feel bad, I read the holy writings and pray to God. He will help you be patient.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Are you angry with me?\u8221" Sal asked.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Not anymore. I love you. Please take the bracelet off now.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Sal removed the bracelet. Crystal disappeared with a whoosh.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Sal tried to wait. She wanted to see her sister grown up and beautiful like Crystal said. She wanted it now.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Even though she didn\u8217't feel like being patient, she prayed for God to help her. She waited for days.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Thirteen beads were dim now. Sal didn\u8217't see the harm in taking a peek inside the cocoon. It was only a day early.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Sal inserted her little fingers between two silk strands near the top of the cocoon. She pried gently, until the silk gave way.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 When Sal peered into the hole, it was too dark inside for her to see anything.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 She felt bad that she tried to open the cocoon early. Crystal would probably be mad again.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Sal leaned against the wall and cried. She tried to be patient, but she failed.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 When Sal glanced back at the cocoon, she noticed a stream of blood coming from the hole she made. Even when she pressed her hand against the cocoon, the bleeding didn\u8217't stop.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 {\i \u8216'Where is the bracelet?\u8217'} Sal panicked.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 She raced into the kitchen to look for it. Now she knew Crystal would be mad at her.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Once she found the bracelet, she held it over her hand. Should she put it on?\par}
{\pard \ql \f0 \sa180 \li0 \fi0 she put the bracelet on the mantle.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 She took it down again.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Crystal might not want to help her after she disobeyed by putting it on.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 She took a breath and slipped it on anyway.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Crystal thundered into the room the same way as before. Her wings stretched out, making her look very big and mean.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"What is wrong, Sallina?\u8221" Crystal asked.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I tried to look early,\u8221" Sal hung her head.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Crystal charged past Sal into Charlea\u8217's room. Next to the cocoon, she knelt and placed her fingers on the hole. Tears sprang from Crystal\u8217's eyes: strange tears, red, made of blood.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Will she die?\u8221" Sal asked. Her lip quavered.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"She is already dead dear. The question is whether she will come back.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Will she come back, Miss Crystal?\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I don\u8217't know. You hurt your sister very badly. I will try to help her. Pray.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Sal prayed while Crystal worked to save Charlea. Silk flew from Crystal\u8217's wrists while red tears mingled with Charlea\u8217's blood. Grey flames danced around the cocoon as Crystal patched the hole.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Crystal leaned against the wall, exhausted. Sal cried into Crystal\u8217's chest.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Did you save her?\u8221" Sal asked.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I don\u8217't know dear, let\u8217's pray.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Crystal prayed for a long while that God would spare Charlea. Sal was too sad to pray. She just listened until she fell asleep with her head in Crystal\u8217's lap.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 In the morning, soft noises woke Sal up. The cocoon rustled slightly. Crystal was gone.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Sal looked at the bracelet. None of the beads glowed anymore.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Even though she was scared, she went to the cocoon. She tugged on the silk. It came off more easily than a day earlier. Happiness filled Sal. Her sister was alive again.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 A blade came from inside the cocoon and cut the silk. Sal pulled more quickly until all the silk came off.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 A slime-covered woman sat in front of Sal. She had the same brown hair as before, but everything else differed. She wasn\u8217't a monster anymore.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Sal helped Charlea into the kitchen and found that the water for a bath was already drawn. Crystal must have filled it that morning. After Charlea emerged from the water, she was beautiful. Sal helped her into a new dress that Crystal left as a gift.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 As she admired the dress, Sal noticed the scar on Charlea\u8217's face in the same place where she tried to open the cocoon.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Sal told her sister everything, including how she disobeyed Crystal.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I forgive you, Sal; you\u8217're my sister,\u8221" Charlea hugged her.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"But I hurt you,\u8221" Sal said.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I know. I should have told you what would happen before I died, but I didn\u8217't have the energy to.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"You have a scar because of me,\u8221" Sal said.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"I forgive you, you are my best friend. A Silkwing gets to choose two people to be best friends for life. Those people get to have a part of that Silkwing living inside them.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Who will you pick?\u8221" Sal asked.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"You. Would you like that?\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Yes,\u8221" Sal said.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 Charlea\u8217's fangs entered into her sister\u8217's arm and venom surged in. Next, a symbiote from Charlea entered Sal. It hurt at first, but afterwards filled Sal with joy.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Who is the other friend?\u8221" Sal asked.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"The carpenter\u8217's apprentice, Jon. I like him, he\u8217's so kind to me.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Oh, you like him that way. You want him to marry you,\u8221" Sal said with glee.\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Do you think he\u8217'll say yes?\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 \u8220"Definitely.\u8221"\par}
{\pard \ql \f0 \sa180 \li0 \fi0 The two sisters walked hand in hand to Jon\u8217's house. They had never been happier.\par}
}
