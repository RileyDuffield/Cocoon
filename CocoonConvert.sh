#SPDX-FileCopyrightText: 2020 Riley Duffield <NylaWeothief@disroot.org>
#
#SPDX-License-Identifier: CC0-1.0
#
#!/bin/bash

# This script converts the Markdown file into various document formats.

CurrentDate="$(date +%d" "%B" "%Y)"

sed -i "10s/.*/date: $CurrentDate/" title.txt

# Convert Cocoon.md to other formats

pandoc -s -f markdown -t odt -o Rendered/Cocoon.odt Cocoon.md
pandoc -s -f markdown -t rtf -o Rendered/Cocoon.rtf Cocoon.md
cp Cocoon.md Rendered/Cocoon.txt

pandoc -s -f markdown -t epub -o Rendered/Cocoon.epub title.txt Cocoon.md

# Strip metadata from odt

mat2 Rendered/Cocoon.odt
rm Rendered/Cocoon.odt
